class CreateEasyforms < ActiveRecord::Migration[5.1] # :nodoc:
  def change
    create_table :easyforms do |t|
      t.string :receiver,
               null: false,
               comment: 'Email of receiver'
      t.json :form_fields,
             comment: 'Fields of a form in json format'

      t.timestamps
    end
  end
end
