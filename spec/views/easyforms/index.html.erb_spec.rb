require 'rails_helper'

RSpec.describe "easyforms/index", type: :view do
  before(:each) do
    assign(:easyforms, [
      Easyform.create!(
        :receiver => "Receiver",
        :form_fields => ""
      ),
      Easyform.create!(
        :receiver => "Receiver",
        :form_fields => ""
      )
    ])
  end

  it "renders a list of easyforms" do
    render
    assert_select "tr>td", :text => "Receiver".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
