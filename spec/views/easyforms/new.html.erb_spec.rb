require 'rails_helper'

RSpec.describe "easyforms/new", type: :view do
  before(:each) do
    assign(:easyform, Easyform.new(
      :receiver => "MyString",
      :form_fields => ""
    ))
  end

  it "renders new easyform form" do
    render

    assert_select "form[action=?][method=?]", easyforms_path, "post" do

      assert_select "input[name=?]", "easyform[receiver]"

      assert_select "input[name=?]", "easyform[form_fields]"
    end
  end
end
