require 'rails_helper'

RSpec.describe "easyforms/edit", type: :view do
  before(:each) do
    @easyform = assign(:easyform, Easyform.create!(
      :receiver => "MyString",
      :form_fields => ""
    ))
  end

  it "renders the edit easyform form" do
    render

    assert_select "form[action=?][method=?]", easyform_path(@easyform), "post" do

      assert_select "input[name=?]", "easyform[receiver]"

      assert_select "input[name=?]", "easyform[form_fields]"
    end
  end
end
