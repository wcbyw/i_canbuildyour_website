require 'rails_helper'

RSpec.describe "easyforms/show", type: :view do
  before(:each) do
    @easyform = assign(:easyform, Easyform.create!(
      :receiver => "Receiver",
      :form_fields => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Receiver/)
    expect(rendered).to match(//)
  end
end
