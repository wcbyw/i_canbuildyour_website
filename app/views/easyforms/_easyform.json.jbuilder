json.extract! easyform, :id, :receiver, :form_fields, :created_at, :updated_at
json.url easyform_url(easyform, format: :json)
