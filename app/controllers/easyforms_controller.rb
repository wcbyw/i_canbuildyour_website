class EasyformsController < ApplicationController
  before_action :set_easyform, only: [:show, :edit, :update, :destroy]

  # GET /easyforms
  # GET /easyforms.json
  def index
    @easyforms = Easyform.all
  end

  # GET /easyforms/1
  # GET /easyforms/1.json
  def show
  end

  # GET /easyforms/new
  def new
    @easyform = Easyform.new
  end

  # GET /easyforms/1/edit
  def edit
  end

  # POST /easyforms
  # POST /easyforms.json
  def create
    @easyform = Easyform.new(easyform_params)

    respond_to do |format|
      if @easyform.save
        format.html { redirect_to @easyform, notice: 'Easyform was successfully created.' }
        format.json { render :show, status: :created, location: @easyform }
      else
        format.html { render :new }
        format.json { render json: @easyform.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /easyforms/1
  # PATCH/PUT /easyforms/1.json
  def update
    respond_to do |format|
      if @easyform.update(easyform_params)
        format.html { redirect_to @easyform, notice: 'Easyform was successfully updated.' }
        format.json { render :show, status: :ok, location: @easyform }
      else
        format.html { render :edit }
        format.json { render json: @easyform.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /easyforms/1
  # DELETE /easyforms/1.json
  def destroy
    @easyform.destroy
    respond_to do |format|
      format.html { redirect_to easyforms_url, notice: 'Easyform was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_easyform
      @easyform = Easyform.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def easyform_params
      params.require(:easyform).permit(:receiver, :form_fields)
    end
end
